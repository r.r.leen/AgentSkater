﻿using AgentSkater.Agents;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentSkater.Controller
{
    class AgentController
    {

        private int width = 400;
        private int height = 400;
        private int amountOfAgents = 10;
        private int amountOfDirections = 8;
        private int speed = 5;
        private int size = 20;
        private double exploration = 0.2;
        private double learningRate = 0.2;
        private int reward1 = 10;
        private int reward2 = -1000;

        private GreedyLearning q;
        List<SkateAgent> agentList;
        List<Tuple<SkateAgent, SkateAgent>> recentlyHit = new List<Tuple<SkateAgent, SkateAgent>>();
       
        public Form1 form;
       
        public int getGameSizeHeigth()
        {
            return height;
        }
        public int getGameSizeWidth()
        {
            return width;
        }
        public int getReward1()
        {
            return reward1;
        }
        public int getReward2()
        {
            return reward2;
        }
        public void UpdateChart()
        {

            Dictionary<double, double> average = new Dictionary<double, double>();
            List<Tuple<SkateAgent, double, double>>  data = q.getQ();
            foreach(Tuple<SkateAgent,double,double> agent in data)
            {
             
                if (!average.ContainsKey(agent.Item2))
                {
                    average.Add(agent.Item2, agent.Item3);
                }else
                {
                    average[agent.Item2] += agent.Item3;
                }
                
            }

            Dictionary<double, double> temp = new Dictionary<double, double>(average);
            foreach (KeyValuePair<double,double> element in average)
            {
                double a = element.Value / amountOfAgents;
            
                temp[element.Key] = a;
            }

            foreach(var s in form.chart.Series)
            {               
                s.Points.Add(temp[Convert.ToDouble(s.Name)]);
            }
           
        }
        public void SetupChart()
        {
           
            foreach(double d in GenerateDirections()){
                form.chart.Series.Add(d.ToString());
                form.chart.Series[form.chart.Series.Count - 1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            }
            
        }


        public AgentController(Form1 form)
        {
            form.canvas.Width = width;
            form.canvas.Height = height;
            this.form = form;
            this.agentList = new List<SkateAgent>();
            createNewSkateAgent();
            SetupChart();


        }

        public void createNewSkateAgent() {
            for(int x= 0; x < amountOfAgents; x++)
            {
                agentList.Add(new SkateAgent(this, size, speed));
            }         
          
        }

        public List<double> GenerateDirections()
        {
            List<double> directions = new List<double>();
            double stepSize = 360 / amountOfDirections;
            double counter = 0;
            while(counter < 359)
            {
                directions.Add(counter);
                counter = counter + stepSize;
            }
            return directions;
        }
        public void InitializeQleaner()
        {
            this.q = new GreedyLearning(agentList, GenerateDirections(), learningRate , exploration);
        }

        public void InitializeAllAgents()
        {
           
            Random ran = new Random();
            List<Point> locations = new List<Point>();
            foreach (SkateAgent agent in agentList)
            {
                agent.SetQlearner(q);
                bool restart = true;
                int x = 0;
                int y = 0;
                while (restart)
                {
                    x = ran.Next(size, width);
                    y = ran.Next(size, height);
                    if (locations.Count == 0)
                    {
                        locations.Add(new Point(x, y));
                        restart = false;
                    }
                    else
                    {
                        restart = false;
                        foreach (Point p in locations)
                        {
                            if ((Math.Abs(x - p.X) < agent.GetHeight() && Math.Abs(y - p.Y) < agent.GetWidth()))
                            {
                                restart = true;
                            }
                        }
                        if (!restart)
                        {
                            locations.Add(new Point(x, y));
                        }
                    }   
                }
                agent.StartRound(x, y);
            }
        }

        internal void redrawAll()
        {
            form.canvas.Refresh();
            Pen p = new Pen(Color.Blue, 10F);
            Graphics graphics = form.canvas.CreateGraphics();
            foreach (SkateAgent agent in agentList)
            {
              
                graphics.DrawRectangle(p, (float)agent.GetXpos(), (float)agent.GetYpos(), agent.GetWidth(), agent.GetHeight());


               
            }
            form.chart.Update();
        }
        public void Start()
        {

            
            for (int x = 500; x >= 0; x--)
            {
                
                this.MoveAllAgents();
                // this.CheckCollisions();

                //canvas.Refresh();
                if (x % 100 == 0)
                {
                    this.redrawAll();
                }
              //  System.Threading.Thread.Sleep(3);
            }
            Console.Out.WriteLine("-");
            foreach(SkateAgent agent in agentList)
            {
                q.update(agent);
            }
            UpdateChart();




        }

        internal void clear()
        {
            agentList.Clear();
        }

        public void MoveAllAgents()
        {
            foreach (SkateAgent agent in agentList)
            {               
                agent.Move();
            }
        }

        public Boolean CheckCollision(SkateAgent myAgent,double x,double y)
        {
            foreach (SkateAgent agent in agentList)
            {
                if (myAgent != agent &&
                      (Math.Abs(agent.GetXpos() -x) < Math.Max(agent.GetWidth(), myAgent.GetWidth()) &&
                      Math.Abs(agent.GetYpos() - y) < Math.Max(agent.GetHeight(), myAgent.GetHeight())))
                {
                    return true;
                }
            }
            return false;
           
        }
        /*
        public void CheckCollisions()
        {
            List<SkateAgent> agentCopyList = agentList;
            List<Tuple<SkateAgent, SkateAgent>> tempRecentlyHit = new List<Tuple<SkateAgent, SkateAgent>>();

            for (int counterX = 0; agentList.Count > counterX; counterX++)
            {
                for (int counterY = counterX +1; agentCopyList.Count > counterY; counterY++)
                {
                    if (agentCopyList[counterY] != agentList[counterX] &&
                        (Math.Abs(agentList[counterX].GetXpos() - agentCopyList[counterY].GetXpos()) < Math.Max(agentList[counterX].GetWidth(), agentCopyList[counterY].GetWidth()) &&
                        Math.Abs(agentList[counterX].GetYpos() - agentCopyList[counterY].GetYpos()) < Math.Max(agentList[counterX].GetHeight(), agentCopyList[counterY].GetHeight())))
                    {
                        Boolean found = false;
                        foreach (Tuple<SkateAgent, SkateAgent> tuple in recentlyHit)
                        {
                            if (tuple.Item1 == agentList[counterX] && tuple.Item2 == agentCopyList[counterY] || tuple.Item1 == agentCopyList[counterY] && tuple.Item2 == agentList[counterX])
                            {
                                found = true;
                                tempRecentlyHit.Add(tuple);
                            }
                        }
                        if (!found)
                        {
                            agentList[counterX].Hit();
                            agentCopyList[counterY].Hit();
                            recentlyHit.Add(new Tuple<SkateAgent, SkateAgent>(agentList[counterX], agentCopyList[counterY]));
                        }


                    }else
                    {
                        Tuple<SkateAgent, SkateAgent> removetuple = null;

                        foreach (Tuple<SkateAgent, SkateAgent> tuple in recentlyHit)
                        {
                            if (tuple.Item1 == agentList[counterX] && tuple.Item2 == agentCopyList[counterY] || tuple.Item1 == agentCopyList[counterY] && tuple.Item2 == agentList[counterX])
                            {
                                removetuple = tuple;

 
                            }
                        }
                        if (removetuple != null)
                        {
                            recentlyHit.Remove(removetuple);
                        }
                    }
                }
            }
           
          
         
        }*/
    }
}
