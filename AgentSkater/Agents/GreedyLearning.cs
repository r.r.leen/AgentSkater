﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentSkater.Agents
{
    class GreedyLearning
    {
        private List<Tuple<SkateAgent, double, double>> q = new List<Tuple<SkateAgent, double, double>>();
        private double learningRate;
        private int ammountOfAgents;
        private List<double> directions;
        private double exploration;
        private Random rnd = new Random(System.DateTime.Now.Millisecond);

        public GreedyLearning(List<SkateAgent> agents,List<double> directions, double learningRate,double exploration)
        {
            
            this.directions = directions;
            ammountOfAgents = agents.Count;
            this.learningRate = learningRate;
            this.exploration = exploration;
            foreach (SkateAgent agent in agents)
            {
                foreach(double direction in directions)
                {
                    q.Add(new Tuple<SkateAgent, double, double>(agent, direction, 0));
                }
            }

            
        }

        public void update(SkateAgent agent)
        {
            List<Tuple<SkateAgent, double, double>> removeList = new List<Tuple<SkateAgent, double, double>>();
            List<Tuple<SkateAgent, double, double>> addList = new List<Tuple<SkateAgent, double, double>>();
         
               double value = agent.getReward();
            
         
            foreach( Tuple<SkateAgent, double, double> tuple in q)
            {
                if(tuple.Item1 == agent && tuple.Item2 == agent.getLastDirection())
                {
                    removeList.Add(tuple);                    
                    double newvalue = (1 - learningRate) * tuple.Item3 + learningRate * value;
                    addList.Add(new Tuple<SkateAgent, double, double>(tuple.Item1, tuple.Item2, newvalue));
                   
                }
            }
            foreach(Tuple<SkateAgent,double,double> tuple in removeList)
            {
                q.Remove(tuple);
            }
            q.AddRange(addList);


        
        }

        public double getDirection(SkateAgent agent)
        {
         
            double number = rnd.Next(100);
            if(number < exploration * 100)
            {
                return directions[rnd.Next(directions.Count)];
            }else
            {
                double value = 0;
                double direction = -1;
                foreach (Tuple<SkateAgent, double, double> tuple in q)
                {
                    if (tuple.Item1 == agent && tuple.Item3 > value)
                    {
                        value = tuple.Item3;
                        direction = tuple.Item2;

                    }
                }
                if(direction == -1)
                {
                    return directions[rnd.Next(directions.Count)];
                }
                return direction;

            }
       


         //TODO learning  
            return 0;
        }
        
     public List<Tuple<SkateAgent, double, double>> getQ()
        {
            return q;
        }
    }
}
