﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentSkater.Controller;

using System.Drawing;

namespace AgentSkater.Agents
{
    class SkateAgent
    {

        private double x;
        private double y;

        private Color color;
        private int speed;
        private double currentDirection;
        private int width, height;
        private Boolean gotHit = false;
        private int reward = 0;
      
        private GreedyLearning q;
        private AgentController controller;


        public SkateAgent(AgentController controller,int size,int speed)
        {
            this.width = size;
            this.height = size;
            this.controller = controller;
            currentDirection = 0;
             x = 0;
            y = 0;


          
            color = Color.Blue;
            this.speed = speed;

        }
        public void SetQlearner(GreedyLearning q)
        {
            this.q = q;
        }
        public void StartRound(int xpos,int ypos)
        {
            gotHit = false;
            this.x = xpos;
            this.y = ypos;
            currentDirection = q.getDirection(this);
            this.reward = 0;
        }

        


        public void Move()
        {
            
            double degrees = Math.PI * currentDirection / 180;
            double test = (Math.Cos(degrees));
            double newXpos = (Math.Sin(degrees)) * speed + x;
            double newYpos = Math.Cos(degrees) *-1 * speed + y;
            if(newXpos >= controller.getGameSizeWidth())
            {
                newXpos = newXpos - (controller.getGameSizeWidth() + width);
            }
            if (newXpos < 0 - width)
            {
                newXpos = newXpos + controller.getGameSizeWidth();
            }
            if (newYpos  >= controller.getGameSizeHeigth())
            {
                newYpos = newYpos - (controller.getGameSizeHeigth() + height);
            }
            if (newYpos < 0 - height)
            {
                newYpos = newYpos + controller.getGameSizeHeigth();
            }
            if (controller.CheckCollision(this,newXpos,newYpos))
            {
                if (!gotHit)
                {
                    hit();
                }
                
            }
            else
            {
                nohit();


                x = newXpos;
                y = newYpos;
            }
        }

       
        public double GetXpos()
        {
            return x;
        }
        public double GetYpos()
        {
            return y;
        }
        public int GetWidth()
        {
            return width;
        }
        public int GetHeight()
        {
            return height;
        }
        public void SetColor(Color c)
        {
            color = c;
        }
        public void hit()
        {
            gotHit = true;
            reward = reward + controller.getReward2();
           
        }
        public void nohit()
        {
            reward = reward + controller.getReward1();
        }
        public Color GetColor()
        {
            return color;
        }
        public int getReward()
        {
            return reward;
        }
        public double getLastDirection()
        {
            return currentDirection;
        }


      

    }
}
