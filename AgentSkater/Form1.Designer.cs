﻿namespace AgentSkater
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.btn_start = new System.Windows.Forms.Button();
            this.btn_run = new System.Windows.Forms.Button();
            this.lbl_cycles = new System.Windows.Forms.Label();
            this.tb_cycle = new System.Windows.Forms.TextBox();
            this.drawBox = new System.Windows.Forms.PictureBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.drawBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(16, 647);
            this.btn_start.Margin = new System.Windows.Forms.Padding(4);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(100, 28);
            this.btn_start.TabIndex = 1;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // btn_run
            // 
            this.btn_run.Enabled = false;
            this.btn_run.Location = new System.Drawing.Point(16, 612);
            this.btn_run.Margin = new System.Windows.Forms.Padding(4);
            this.btn_run.Name = "btn_run";
            this.btn_run.Size = new System.Drawing.Size(100, 28);
            this.btn_run.TabIndex = 5;
            this.btn_run.Text = "Run";
            this.btn_run.UseVisualStyleBackColor = true;
            this.btn_run.Click += new System.EventHandler(this.btn_run_Click);
            // 
            // lbl_cycles
            // 
            this.lbl_cycles.AutoSize = true;
            this.lbl_cycles.Location = new System.Drawing.Point(126, 612);
            this.lbl_cycles.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_cycles.Name = "lbl_cycles";
            this.lbl_cycles.Size = new System.Drawing.Size(53, 17);
            this.lbl_cycles.TabIndex = 6;
            this.lbl_cycles.Text = "Cycles:";
            // 
            // tb_cycle
            // 
            this.tb_cycle.Location = new System.Drawing.Point(193, 612);
            this.tb_cycle.Margin = new System.Windows.Forms.Padding(4);
            this.tb_cycle.Name = "tb_cycle";
            this.tb_cycle.Size = new System.Drawing.Size(132, 22);
            this.tb_cycle.TabIndex = 7;
            this.tb_cycle.Text = "250";
            // 
            // drawBox
            // 
            this.drawBox.BackColor = System.Drawing.Color.Red;
            this.drawBox.Location = new System.Drawing.Point(602, 12);
            this.drawBox.Margin = new System.Windows.Forms.Padding(0);
            this.drawBox.Name = "drawBox";
            this.drawBox.Size = new System.Drawing.Size(800, 615);
            this.drawBox.TabIndex = 0;
            this.drawBox.TabStop = false;
            // 
            // chart1
            // 
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(12, 12);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(581, 474);
            this.chart1.TabIndex = 8;
            this.chart1.Text = "chart1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1914, 697);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.tb_cycle);
            this.Controls.Add(this.lbl_cycles);
            this.Controls.Add(this.btn_run);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.drawBox);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.drawBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Button btn_run;
        private System.Windows.Forms.Label lbl_cycles;
        private System.Windows.Forms.TextBox tb_cycle;
        private System.Windows.Forms.PictureBox drawBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}

