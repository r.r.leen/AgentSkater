﻿using AgentSkater.Agents;
using AgentSkater.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace AgentSkater
{
    public partial class Form1 : Form
    {
        public Chart chart;
        public PictureBox canvas;
        private AgentController controller;
        public Form1()
        {
            InitializeComponent();
            canvas = drawBox;
            chart = chart1;
            this.controller = new AgentController(this);
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
          
            //controller.agentMoveAmount = Int32.Parse(tb_stepSize.Text);
            //controller.colissionSize = Int32.Parse(tb_colissionSize.Text);
   
            controller.InitializeQleaner();
            btn_run.Enabled = true;
            btn_start.Enabled = false;
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            this.controller.clear();
           
            canvas.Refresh();
            btn_start.Enabled = true;
        }

        private void btn_run_Click(object sender, EventArgs e)
        {
            btn_start.Enabled = false;
            int amount = Int32.Parse(tb_cycle.Text);
            while (amount > 0)
            {
                controller.InitializeAllAgents();
                controller.Start();
                amount--;
            }

           
           // btn_start.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
    }
}
